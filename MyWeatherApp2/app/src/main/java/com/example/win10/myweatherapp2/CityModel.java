package com.example.win10.myweatherapp2;

/**
 * Created by win 10 on 4/12/2018.
 */

public class CityModel {

    private String city;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
