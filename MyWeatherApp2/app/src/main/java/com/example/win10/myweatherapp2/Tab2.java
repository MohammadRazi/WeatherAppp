package com.example.win10.myweatherapp2;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by win 10 on 4/10/2018.
 */

public class Tab2 extends android.support.v4.app.Fragment {
    ArrayList<String> mylist;
    RecyclerView recyclerView;
    List<String> temp;
    Button ss;
    int positions;
    String listCount;
    ListView listView;
    ArrayList list_item = new ArrayList<>();
    static String my_sel_items;
    private String lv_items[] = {"Tehran", "kabul", "karaj", "London"};
    ApiService2 api;

    public Tab2() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.tab2, container, false);
        //recyclerView= (RecyclerView) view.findViewById(R.id.recyclerView_Weather);
        listView = (ListView) view.findViewById(R.id.listView);
        api = new ApiService2(getContext());

        ss = (Button) view.findViewById(R.id.ss);

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_multiple_choice, lv_items);
        listView.setAdapter(arrayAdapter);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                positions=position;
                listView.getAdapter().getItem(position).toString();





            }
        });

        ss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listView.isItemChecked(positions)) {


                    Log.i("aaa", "onItemClick: "+listView.getCheckedItemPositions() );
                    for (int i = 0; i < lv_items.length; i++) {
                        if (listView.isItemChecked(i)) {
                           // api.getCurrentWeather(listView.getAdapter().getItem(i).toString());
                            api.getCurrentWeather(new ApiService2.OnWeatherInfoRecieved() {
                                @Override
                                public void onRecieved(DataModel datamodel) {

                                }
                            },  listView.getAdapter().getItem(positions).toString() );
                        }
                    }


                }
            }

        });
//        Toast.makeText(getContext(), mylist.get(1), Toast.LENGTH_LONG).show();


        return view;

    }

}