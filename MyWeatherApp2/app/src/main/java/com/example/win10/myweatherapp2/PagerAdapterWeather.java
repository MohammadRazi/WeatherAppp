package com.example.win10.myweatherapp2;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.widget.RecyclerView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;


public class PagerAdapterWeather extends FragmentPagerAdapter {
    TabCurrentWeather tab;

    private static List<Fragment> myFragment = new ArrayList<>();
    private static List<Integer> mydegre = new ArrayList<>();
    private static List<Integer> myPressure = new ArrayList<>();
    private static List<Integer> myId = new ArrayList<>();
    private static List<String> myCityName = new ArrayList<>();


    public void AddFragment(Fragment frag, int degre, int pressure, int id, String cityName) {
        myFragment.add(frag);
        mydegre.add(degre);
        myPressure.add(pressure);
        myId.add(id);
        myCityName.add(cityName);
    }
    public void AddSecondFragment(Fragment frag) {
      myFragment.add(frag);


    }
    public void AddThirdFragment(Fragment frag) {
        myFragment.add(frag);


    }



    private String[] titles = {"Current", "ListOfWeather", "Cities"};

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }

    public PagerAdapterWeather(FragmentManager fm) {
        super(fm);




    }

    @Override
    public Fragment getItem(int position) {


        switch (position) {

            case 0:
                return myFragment.get(position);
            default: return myFragment.get(position);


        }
    }


    @Override
    public int getCount() {
        return 3;
    }





}

