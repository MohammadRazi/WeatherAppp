package com.example.win10.myweatherapp2;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

/**
 * Created by win 10 on 4/9/2018.
 */

public class TabCurrentWeather extends android.support.v4.app.Fragment {
    int degree_num;
    String cityMyName;
    int id , temp , pressure;
    Button btn;
    String cityName,Search_cityName="london";
    TextView tv,tv_pressure,tv_id,tv_city;
    ImageView img_weather;
    SearchableSpinner spinner;



    public TabCurrentWeather() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_current_weather, container, false);
        tv = (TextView) view.findViewById(R.id.degree_current_number);
        tv_id = (TextView) view.findViewById(R.id.tv_id);
        tv_city = (TextView) view.findViewById(R.id.cityName);
        tv_pressure = (TextView) view.findViewById(R.id.tv_pressure);
        btn = (Button) view.findViewById(R.id.btn_req);
        img_weather = (ImageView) view.findViewById(R.id.img_view_weather);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getdata();
            }
        });

                        /*Log.i("degreeeeeeeeeeeeeeeeee", "onRecieved: "+degree_num);
                        Log.i("degreeeeeeeeeeeeeeeeee", "onRecieved: "+pressure);
                        Log.i("degreeeeeeeeeeeeeeeeee", "onRecieved: "+cityName);
                        Log.i("degreeeeeeeeeeeeeeeeee", "onRecieved: "+id);
*/


        /*btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ApiService apiservice = new ApiService(getContext());
                apiservice.getCurrentWeather(new ApiService.OnWeatherInfoRecieved() {
                    @Override
                    public void onRecieved(DataModel datamodel) {


                        degree_num = datamodel.getDegree_current_num();
                        pressure = datamodel.getPressure();
                        cityName = datamodel.getCityName();
                        id = datamodel.getId();



                        *//*Log.i("degreeeeeeeeeeeeeeeeee", "onRecieved: "+degree_num);
                        Log.i("degreeeeeeeeeeeeeeeeee", "onRecieved: "+pressure);
                        Log.i("degreeeeeeeeeeeeeeeeee", "onRecieved: "+cityName);
                        Log.i("degreeeeeeeeeeeeeeeeee", "onRecieved: "+id);
*//*
                        switch (id / 100) {
                            case 2:
                                img_weather.setImageResource(R.drawable.thinderstrom);
                                tv_id.setText(String.valueOf("Thunderstorm"));
                                break;
                            case 3:
                                img_weather.setImageResource(R.drawable.drizzle);
                                tv_id.setText(String.valueOf("Drizzle"));
                                break;
                            case 5:
                                img_weather.setImageResource(R.drawable.rain);
                                tv_id.setText(String.valueOf("Rain"));
                                break;
                            case 6:
                                img_weather.setImageResource(R.drawable.snow);
                                tv_id.setText(String.valueOf("Snow"));
                                break;
                            case 7:
                                img_weather.setImageResource(R.drawable.atmosphere);
                                tv_id.setText(String.valueOf("Atmosphere"));
                                break;
                            case 8:
                                img_weather.setImageResource(R.drawable.sunny);
                                tv_id.setText(String.valueOf("Clear"));
                                break;

                            default:
                                img_weather.setImageResource(R.drawable.sunny);
                                tv_id.setText(String.valueOf("Clear"));
                                break;

                        }
                        tv.setText((String.valueOf((int)(degree_num - 272.15))));
                        tv_pressure.setText(String.valueOf("Pressure : "+pressure+"hPa"));
                        tv_city.setText(cityName);

                    }
                });
            }
        });*/
        return view;
    }



    public void getdata() {

        ApiService2 apiService = new ApiService2(getContext());
        apiService.getCurrentWeather(new ApiService2.OnWeatherInfoRecieved() {
            @Override
            public void onRecieved(DataModel datamodel) {
                temp =datamodel.getTemp();
                id = datamodel.getId();
                pressure = datamodel.getPressure();
                cityMyName = datamodel.getCityName();


                tv.setText((String.valueOf((int) (temp - 272.15))));


                tv_pressure.setText(String.valueOf("Pressure : " + pressure + "hPa"));
                tv_city.setText(cityMyName);
                tv.setText((String.valueOf((int) (temp - 272.15))));

                switch (id / 100) {
                    case 2:
                        img_weather.setImageResource(R.drawable.thinderstrom);
                        tv_id.setText(String.valueOf("Thunderstorm"));
                        break;
                    case 3:
                        img_weather.setImageResource(R.drawable.drizzle);
                        tv_id.setText(String.valueOf("Drizzle"));
                        break;
                    case 5:
                        img_weather.setImageResource(R.drawable.rain);
                        tv_id.setText(String.valueOf("Rain"));
                        break;
                    case 6:
                        img_weather.setImageResource(R.drawable.snow);
                        tv_id.setText(String.valueOf("Snow"));
                        break;
                    case 7:
                        img_weather.setImageResource(R.drawable.atmosphere);
                        tv_id.setText(String.valueOf("Atmosphere"));
                        break;
                    case 8:
                        img_weather.setImageResource(R.drawable.sunny);
                        tv_id.setText(String.valueOf("Clear"));
                        break;

                    default:
                        img_weather.setImageResource(R.drawable.sunny);
                        tv_id.setText(String.valueOf("Clear"));
                        break;

                }






            }
        }, WeatherTabsView.A);


    }






    }




