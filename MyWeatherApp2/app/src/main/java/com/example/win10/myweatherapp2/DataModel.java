package com.example.win10.myweatherapp2;

/**
 * Created by win 10 on 4/9/2018.
 */

public class DataModel {
    private String cityName;
    private int temp;
    private int pressure;
    private int id;



    public int getDegree_current_num() {
        return temp;
    }

    public void setDegree_current_num(int temp) {
        this.temp = temp;
    }

    public int getTemp() {
        return temp;
    }

    public void setTemp(int temp) {
        this.temp = temp;
    }

    public int getPressure() {
        return pressure;
    }

    public void setPressure(int pressure) {
        this.pressure = pressure;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

}
