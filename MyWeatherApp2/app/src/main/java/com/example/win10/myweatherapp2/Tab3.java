package com.example.win10.myweatherapp2;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by win 10 on 4/13/2018.
 */

public class Tab3 extends Fragment implements ApiService2.OnWeatherInfoRecieved{

    TextView tv;
    ApiService2 apiService;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.city_list, container, false);
        tv = (TextView) view.findViewById(R.id.tv_tab3);
        apiService = new ApiService2(getContext());
        apiService.getCurrentWeather(this,"tehran");


        return view;
    }

    @Override
    public void onRecieved(DataModel datamodel) {


    }

}
