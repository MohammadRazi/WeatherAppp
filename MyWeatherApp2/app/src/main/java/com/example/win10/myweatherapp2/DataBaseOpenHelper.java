package com.example.win10.myweatherapp2;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by win 10 on 4/20/2018.
 */

public class DataBaseOpenHelper extends SQLiteOpenHelper {

    public static final int DATA_BASE_VERSION = 1;
    public static final String DATA_BASE_NAME = "db_mohammad";
    public static final String TABLE_NAME = "weather";



    private static final String COL_ID="col_id";
    private static final String COL_TEMP ="col_title";
    private static final String COL_CITY="col_city";

    private static final String SQL_COMMAND_CREATE_POST_TABLE="CREATE TABLE IF NOT EXISTS "+TABLE_NAME+"("+
            COL_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+
            COL_TEMP +" TEXT,"+
            COL_CITY+" TEXT);";



    public DataBaseOpenHelper(Context context) {
        super(context, DATA_BASE_NAME, null, DATA_BASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_COMMAND_CREATE_POST_TABLE);

    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public boolean addPost(DataModel dataModel){
        ContentValues cv=new ContentValues();
        cv.put(COL_TEMP,dataModel.getTemp());
        cv.put(COL_CITY,dataModel.getCityName());


        SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();
        long isInserted=sqLiteDatabase.insert(TABLE_NAME,null,cv);

        Log.i("HHHHH", "addPost: "+isInserted);

        if (isInserted>0){
            return true;
        }else{
            return false;
        }
    }


}
