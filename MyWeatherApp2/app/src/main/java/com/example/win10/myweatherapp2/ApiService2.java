package com.example.win10.myweatherapp2;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by win 10 on 4/9/2018.
 */

public class ApiService2 {
    private int Id,temp,pressure;
    private String cityMyName;
    //private String cityName ;
    private Context context;
    TabCurrentWeather tab;

    private JSONObject response;


    public ApiService2(Context context) {
        this.context = context;
    }


    public void getCurrentWeather(final OnWeatherInfoRecieved onWeatherInfoRecieved,String city) {

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, "http://api.openweathermap.org/data/2.5/weather?q=" + city + "&apikey=86f06c9c25a6f743f7fa83b72c7134fb", null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i("mhmd", "onResponse: " + response.toString());
                onWeatherInfoRecieved.onRecieved(parseResponseToWeatherInfo(response));
                //   parseResponseToWeatherInfo(response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", "onErrorResponse: " + error.toString());

            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(8000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(request);


    }


    private DataModel parseResponseToWeatherInfo(JSONObject response) {
        DataModel datamodel = new DataModel();
        try {
            JSONObject mainJsonObject = response.getJSONObject("main");
            datamodel.setDegree_current_num(mainJsonObject.getInt("temp"));
            JSONArray array = response.getJSONArray("weather");
            JSONObject weatherObject = array.getJSONObject(0);
            datamodel.setId(weatherObject.getInt("id"));
            datamodel.setTemp(mainJsonObject.getInt("temp"));
            datamodel.setPressure(mainJsonObject.getInt("pressure"));


            // "name : london" in json is not object or array
            JSONObject jo = response;
            datamodel.setCityName(jo.getString("name"));


            return datamodel;

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }


    }


    public interface OnWeatherInfoRecieved {
        void onRecieved(DataModel datamodel);

    }

}






